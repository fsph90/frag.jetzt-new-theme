export const blue = {

// landing page buttons and room buttons
  '--primary' : '#5d4037',

  // recently visited Sessions
  '--primary-variant': '#b2fab4',
// frag.jetzt, "create Session" Button, "Session was succesfully created popup and copy to Clipoboard button
  '--secondary': '#004680',
  '--secondary-variant': '#6f74dd',
// background color
  '--background': '#81c784',
  // footer, header and inputfield color
  '--surface': '#b2fab4',
  // intro, data protection and imprint background color
  '--dialog': '#b4ffff',
  // all "cancel" and "close" button colors
  '--cancel': '#26343c',
  // answering question text field
  '--alt-surface': '#b4ffff',
  '--alt-dialog': '#455a64',
// text color on landing page buttons
  '--on-primary': '#FFFFFF',
  //  text color on popups, save buttons
  '--on-secondary': '#FFFFFF',
  '--on-background': '#FFFFFF',
  // all text on Background thats usually white
  '--on-surface': '#000000',
  // all "cancel" and "close" text colors
  '--on-cancel': '#FFFFFF',
// mark as correct symbol
  '--green': '#007070',
  // mark as wrong symbol
  '--red': 'red',
  // mark as bonus question symbol
  '--yellow': '#cc33ff',
  '--blue': '#3f51b5',
  '--purple': '#9c27b0',
  '--light-green': '#80ba24',
  '--grey': '#BDBDBD',
  '--grey-light': '#9E9E9E',
  '--black': '#212121',
  // moderating questions
  '--moderator': '#b4ffff'
};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Nature',
      'de': 'Natur'
    },
    'description': {
      'en': 'Contrast compliant with WCAG 2.1 AA',
      'de': 'Theme nach WCAG 2.1 AA'
    }
  },
  'isDark': false,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'background'

};
